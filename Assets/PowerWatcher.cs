﻿using UnityEngine;
using UnityEngine.UI;

public class PowerWatcher : MonoBehaviour
{
	private Slider _slider;
	private MetalDeformer _deformer;
	// Use this for initialization
	void Start()
	{
		_slider = GetComponent<Slider>();
		_deformer = FindObjectOfType<MetalDeformer>();
	}

	// Update is called once per frame
	void Update()
	{
		_slider.minValue = _deformer.MinHammerPower;
		_slider.maxValue = _deformer.MaxHammerPower;
		_slider.value = _deformer.HammerPower;
	}
}
