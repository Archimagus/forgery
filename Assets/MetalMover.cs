﻿using UnityEngine;

public class MetalMover : MonoBehaviour
{
	public float RotationSpeed;

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		transform.Rotate(0, 0, Input.GetAxis("Vertical") * RotationSpeed * Time.deltaTime, Space.Self);
		transform.Rotate(Input.GetAxis("Pitch") * RotationSpeed * Time.deltaTime, 0, 0, Space.Self);
		transform.Rotate(0, Input.GetAxis("Horizontal") * RotationSpeed * Time.deltaTime, 0, Space.World);
	}
}
