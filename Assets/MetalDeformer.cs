﻿using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshCollider))]
public class MetalDeformer : MonoBehaviour
{
	public float HammerSize = 0.01f;
	public float FalloffPower = 50;
	public float MinHammerPower = 0;
	public float MaxHammerPower = 10;
	public float HammerPowerSpeed = 10;

	public float HammerPower { get; set; }

	private Mesh _mesh;
	private MeshCollider _collider;
	void Start()
	{
		_mesh = GetComponent<MeshFilter>().mesh;
		_collider = GetComponent<MeshCollider>();
	}
	void Update()
	{
		if (Input.GetMouseButton(0))
		{
			HammerPower += HammerPowerSpeed * Time.deltaTime;
			HammerPower = Mathf.Clamp(HammerPower, MinHammerPower, MaxHammerPower);
		}
		if (Input.GetMouseButtonUp(0))
		{
			var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit))
			{
				if (hit.collider.gameObject == this.gameObject)
				{
					DeformMesh(hit, ray.direction);
				}
			}
			HammerPower = 0;
		}
	}

	private void DeformMesh(RaycastHit hit, Vector3 hitVector)
	{
		var hitPoint = hit.point;
		var ti = hit.triangleIndex;
		var triangles = _mesh.triangles;
		var vertices = _mesh.vertices;
		var normals = _mesh.normals;

		var n1 = normals[triangles[hit.triangleIndex * 3 + 0]];
		var n2 = normals[triangles[hit.triangleIndex * 3 + 1]];
		var n3 = normals[triangles[hit.triangleIndex * 3 + 2]];


		hitVector.Normalize();

		hitPoint = transform.InverseTransformPoint(hitPoint);
		//var hitDir = transform.InverseTransformDirection(hitVector);
		var hitDir = -(n1 + n2 + n3) / 3;
		for (int i = 0; i < vertices.Length; i++)
		{
			var v = vertices[i];
			var dist = Mathf.Clamp01(Vector3.Distance(v, hitPoint));
			var hitPower = Mathf.Clamp01(Mathf.Pow((1 + HammerSize) - dist, FalloffPower)) * 0.002f * HammerPower;

			vertices[i] += hitDir * hitPower;
		}
		_mesh.vertices = vertices;
		_collider.sharedMesh = _mesh;
		_mesh.RecalculateNormals();
	}
}